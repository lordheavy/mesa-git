pkgbase=lib32-llvm
pkgname=('lib32-llvm-libs-git' 'lib32-llvm-git')
pkgdesc='Low Level Virtual Machine (32-bit)(git version)'
pkgver=17.0.0_r465975.f5f5c4726ca8
pkgrel=1
groups=('mesagit')
arch=('x86_64')
url="http://llvm.org"
license=('custom:University of Illinois/NCSA OSL')
makedepends=('cmake' 'lib32-libffi' 'python' 'lib32-zlib' 'lib32-zstd' 'subversion' 'gcc'
             'lib32-libxml2' 'ninja' 'git')
options=('staticlibs' '!emptydirs' '!debug')
source=('llvm-project-git::git+https://github.com/llvm/llvm-project.git'
        llvm.patch)
md5sums=('SKIP'
         'e210a716cd7b0c0db29cd3c654fc5ce2')

pkgver() {
    cd llvm-project-git/llvm

    # This will almost match the output of `llvm-config --version` when the
    # LLVM_APPEND_VC_REV cmake flag is turned on. The only difference is
    # dash being replaced with underscore because of Pacman requirements.
    local _pkgver=$(awk -F 'MAJOR |MINOR |PATCH |)' \
            'BEGIN { ORS="." ; i=0 } \
             /set\(LLVM_VERSION_/ { print $2 ; i++ ; if (i==2) ORS="" } \
             END { print "\n" }' \
             CMakeLists.txt)_r$(git rev-list --count HEAD).$(git rev-parse --short HEAD)
    echo "${_pkgver}"
}

prepare() {
  cd llvm-project-git

  # cp -R clang llvm/tools/clang

  rm -rf build && mkdir build
  rm -rf "${srcdir}"/fakeinstall

  #patch -Np1 -i "$srcdir"/llvm.patch
}


build() {
  cd "$srcdir"/llvm-project-git/build

  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"
  export CXXFLAGS="${CXXFLAGS} -fno-lto"
  export CFLAGS="${CFLAGS} -fno-lto"

  cmake ../llvm -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLLVM_HOST_TRIPLE=$CHOST \
    -DLLVM_LIBDIR_SUFFIX=32 \
    -DCMAKE_C_FLAGS:STRING=-m32 \
    -DCMAKE_CXX_FLAGS:STRING=-m32 \
    -DLLVM_TARGET_ARCH:STRING=i686 \
    -DLLVM_DEFAULT_TARGET_TRIPLE="i686-pc-linux-gnu" \
    -DLLVM_BUILD_LLVM_DYLIB=ON \
    -DLLVM_LINK_LLVM_DYLIB=ON \
    -DLLVM_ENABLE_RTTI=ON \
    -DLLVM_ENABLE_LIBEDIT=OFF \
    -DLLVM_ENABLE_FFI=OFF \
    -DLLVM_BUILD_DOCS=OFF \
    -DLLVM_BUILD_EXAMPLES=OFF \
    -DLLVM_ENABLE_SPHINX=OFF \
    -DLLVM_ENABLE_DOXYGEN=OFF \
    -DLLVM_APPEND_VC_REV=OFF \
    -DFFI_INCLUDE_DIR=$(pkg-config --variable=includedir libffi) \
    -DLLVM_BINUTILS_INCDIR=/usr/include
    
  ninja all
  
  DESTDIR="${srcdir}/fakeinstall" ninja install
}

_fakeinstall() {
  local src f dir
  for src; do
    f="${src#fakeinstall/}"
    dir="${pkgdir}/${f%/*}"
    install -m755 -d "${dir}"
    mv -v "${src}" "${dir}/"
  done
}

package_lib32-llvm-libs-git() {
  pkgdesc='Low Level Virtual Machine library (runtime library)(32-bit)(git version)'
  depends=('lib32-libffi' 'lib32-zlib' 'lib32-libxml2' 'lib32-gcc-libs')
  provides=('lib32-llvm-libs')
  replaces=('lib32-llvm-libs-svn')
  conflicts=('lib32-llvm-libs-svn')

  _fakeinstall fakeinstall/usr/lib32/libLLVM-*.so
  _fakeinstall fakeinstall/usr/lib32/libRemarks.so.*

  install -Dm644 "$srcdir"/llvm-project-git/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_lib32-llvm-git() {
  pkgdesc='Low Level Virtual Machine (32-bit)(git version)'
  depends=("lib32-llvm-libs-git=${pkgver}" 'llvm-git')
  provides=('lib32-llvm')
  replaces=('lib32-llvm-svn' 'llvm-svn')
  conflicts=('lib32-llvm' 'lib32-llvm-svn')

  _fakeinstall fakeinstall/usr/lib32
  # Remove libs which conflict with llvm-libs
  rm -f "$pkgdir"/usr/lib32/{libLLVM,libLTO,LLVMgold,libRemarks}.so

  _fakeinstall fakeinstall/usr/bin/llvm-config
  mv -v "$pkgdir"/usr/bin/llvm-config "$pkgdir"/usr/lib32/llvm-config

  rm -rf "$pkgdir"/usr/{bin/*,include,share/{doc,man,llvm,opt-viewer}}
  mv -v "$pkgdir"/usr/lib32/llvm-config "$pkgdir"/usr/bin/llvm-config32
  
  # make sure there are no files left to install
  find fakeinstall -depth -print0 | xargs -0 rm -r

  install -Dm644 "$srcdir"/llvm-project-git/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# package_lib32-clang-svn() {
#   pkgdesc="C language family frontend for LLVM (32-bit)(svn version)"
#   url="http://clang.llvm.org/"
#   depends=("lib32-llvm-svn=${pkgver}" 'clang-svn' 'gcc-multilib')
#   provides=('lib32-clang')
# 
#   cd llvm
# 
#   make -C build/tools/clang DESTDIR="$pkgdir" install
# 
#   rm -rf "$pkgdir"/usr/{bin,include,share,libexec}
# 
#   install -Dm644 "$srcdir"/llvm-project-git/llvm/LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
# }
# 
